# Tipsport fullscreen live

## Watch live stream on fullscreen!


For automatically fullscreened popup you only need this Chrome extension: https://chrome.google.com/webstore/detail/custom-javascript-for-web/poakhlngfciodnhlhhgnaaelnpjljija
and code below.

For manual, putting into console, just paste code.


```javascript

function see_live_fullscreen() {
    document.getElementsByClassName('m-scoreboardStream')[0].style='max-width: 1920px !important;max-height: 1080px !important;';
    document.getElementsByClassName('m-scoreboardStream__streamBlock')[0].style='max-width: 1920px !important;max-height: 1080px !important;';
    document.getElementsByClassName('m-streamPopup__header')[0].style='display:none !important;';
    window.resizeTo(1920, 1080);
    var el = document.documentElement
        , rfs = // for newer Webkit and Firefox
               el.requestFullScreen
            || el.webkitRequestFullScreen
            || el.mozRequestFullScreen
            || el.msRequestFullscreen
    ;
    if(typeof rfs !="undefined" && rfs){
      rfs.call(el);
    } else if(typeof window.ActiveXObject != "undefined"){
      // for Internet Explorer
      var wscript = new ActiveXObject("WScript.Shell");
      if (wscript != null) {
         wscript.SendKeys("{F11}");
      }
    }
}

var docUrl = document.URL;
var results = docUrl.match(/tipsport.cz\/live\/([^\?]+)(\?.*)?\/popup/g);

if (results.length > 0) {
    see_live_fullscreen();
} else {
    console.log('TIPSPORT LIVE: not found');
}

```